import React from 'react'


const Signup = () => {
  

  return (
    <form>
      <h3>Signup</h3>
      <label>First Name</label>
      <input type="text" />

      <label>Last Name</label>
      <input type="text" />

      <label>Email</label>
      <input type="email" />

      <label>Password</label>
      <input type="password" />

      <button>Sign Up</button>

      <h3>Already a user?</h3>
      <button>Login</button>
    </form>
  )
}

export default Signup