import React, { useState, useContext } from 'react'
import UserContext from '../UserContext'

const Login = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isActive, setIsActive] = useState(false)

  const { user, setUser} = useContext(UserContext)

  return (
    <form>
      <h3>Login</h3>
      <label>Email</label>
      <input type="text" />

      <label>Password</label>
      <input type="password" />

      <button>Login</button>

      <h3>Want to join us?</h3>
      <button>Create an account</button>
    </form>
  )
}

export default Login